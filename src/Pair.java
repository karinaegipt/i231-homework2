/** Local class to test stability. */
class Pair implements Comparable<Pair> {

   private int primo;
   private int secundo;

   Pair (int a, int b) {
      primo=a;
      secundo=b;
   }

   public int getPrimo() {
      return primo;
   }

   public int getSecundo() {
      return secundo;
   }

   @Override
   public String toString() {
      return "(" + String.valueOf(primo) + ","
         + String.valueOf(secundo) + ")";
   }

   public int compareTo (Pair o) {
      int esim = this.primo;
      int teine = o.primo;
      if (esim > teine)
         return 1;
      else
         if (esim < teine)
            return -1;
         else return 0;
   }
} // Pair
